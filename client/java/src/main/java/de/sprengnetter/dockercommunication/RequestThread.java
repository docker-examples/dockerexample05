package de.sprengnetter.dockercommunication;

public class RequestThread implements Runnable {
    @Override
    public void run() {
        while (true) {
            CounterClient client = new CounterClient();
            try {
                Thread.sleep(5000);
                System.out.println("Answer from Service: Counter = " + client.getCurrentCounterFromService());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
