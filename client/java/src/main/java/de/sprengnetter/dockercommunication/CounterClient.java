package de.sprengnetter.dockercommunication;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

class CounterClient {

    private static final String SERVICE_URL = "http://counterservice:8080/counter";

    private Client client = ClientBuilder.newClient();

    String getCurrentCounterFromService() {
        return client
                .target(SERVICE_URL)
                .request(MediaType.APPLICATION_JSON)
                .get(String.class);
    }
}
