package de.sprengnetter.dockercommunication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockercommunicationApplication {

	public static void main(String[] args) {
		SpringApplication.run(DockercommunicationApplication.class, args);
		RequestThread requestThread = new RequestThread();
		requestThread.run();
	}
}
