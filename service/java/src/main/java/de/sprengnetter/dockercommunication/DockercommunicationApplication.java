package de.sprengnetter.dockercommunication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class DockercommunicationApplication {

	public static void main(String[] args) {
		SpringApplication.run(DockercommunicationApplication.class, args);
        CounterThread counterThread = new CounterThread();
        counterThread.run();
	}
}
