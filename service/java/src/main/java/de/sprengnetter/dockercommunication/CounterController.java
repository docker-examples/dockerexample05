package de.sprengnetter.dockercommunication;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/counter")
@Produces("application/json")
public class CounterController {

    @Path("/")
    @GET
    public String getCurrentCount() {
        return String.valueOf(CounterThread.counter);
    }

}
