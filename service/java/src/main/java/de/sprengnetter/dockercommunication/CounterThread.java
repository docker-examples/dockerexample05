package de.sprengnetter.dockercommunication;

public class CounterThread implements Runnable {

    static int counter = 0;

    @Override
    public void run() {
        while (true) {
            System.out.println(++counter);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
