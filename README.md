**Quellcode**
                                                                                                                                                
Zu finden in Ordnern _client_ (Anfragender Service) und _service_ (Zaehl-Service)

Beispiel dient dazu zu zeigen, wie Container miteinander kommunizieren.

Container werden standarmaessig im Docker0-Bridge-Network hinzugefuegt. Container in diesem Network koennen nur via IP miteinander kommunizieren.

Container die im selben Bridge-Network sind (mit Ausnahme des Default-Docker-Networks!), koennen via Namensaufloesung miteinander kommunizieren

docker-compose kann verwendet werden um mehrere Container/Networks/Volumes von zentraler Stelle aus zu erstellen/starten. docker-compose muss separat zu docker installiert werden und stellt auch die Grundlage fuer docker swarm mode (also fuer sogenannte docker stacks)  dar.

**Docker**

Dockerfiles zu finden unter client bzw. service.

docker-compose.yml auf top level Ebene zu finden.

**Befehle**

**1. Default-Docker0-Bridge-Network (ohne Namensaufloesung)**

Image bauen (service): `docker build -t dockerdemo/docker05service:latest .` (im service-Ordner)

Image bauen (client): `docker build -t dockerdemo/docker05client:latest .` (im client-Ordner)

Container starten (service): `docker run --rm -d --name counterservice dockerdemo/docker05service:latest`

Container starten (client): `docker run --rm -d --name counterclient dockerdemo/docker05client:latest`

Ausgaben von client-Container beobachten: `docker logs -f <client container hash>` --> UnknownHostnameExceptions weil Namensaufloesung nicht funktioniert

In client-Container shell oeffnen: `docker exec -it <client container hash> /bin/sh`

In client-Container curl installieren: `apk add curl`

In client-Container gegen service-IP curlen: `curl http://<Service-IP>:8080/counter` --> IP des service-containers kann rausgefunden werden mit (Achtung: auf Host ausfuehren, nicht im Container!) `docker network ls` und `docker network inspect <ID des bridge networks>` --> Service liefert aktuellen Counter-Stand

Container beenden (client): `docker stop <client container hash>` ODER `docker rm -f <client container hash>`

Container beenden (service): `docker stop <service container hash>` ODER `docker rm -f <service container hash>`

**2. Neu erstelltes Bridge-Network (mit Namensaufloesung)**

Network erstellen: `docker network create -d bridge counter_nw`

anschliessend wie in 1, also:

ggfs. Image bauen (service): `docker build -t dockerdemo/docker05service:latest .` (im service-Ordner)

ggfs. Image bauen (client): `docker build -t dockerdemo/docker05client:latest .` (im client-Ordner)

Container starten (service): `docker run --rm -d --name counterservice dockerdemo/docker05service:latest`

Container starten (client): `docker run --rm -d --name counterclient dockerdemo/docker05client:latest`

Ausgaben von client-Container beobachten: `docker logs -f <client container hash>` --> funktioniert nun mit container-Namen

Container beenden (client): `docker stop <client container hash>` ODER `docker rm -f <client container hash>`

Container beenden (service): `docker stop <service container hash>` ODER `docker rm -f <service container hash>`

Network loeschen: docker network rm <network ID> (`docker network ls`)

**3. Verwendung von docker-compose**

auf top level Ebene: `docker-compose up --build`